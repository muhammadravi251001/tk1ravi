from django import forms
from .models import DampakBerita

class DampakBeritaForm(forms.ModelForm):
	class Meta:
		model = DampakBerita
		fields = '__all__'

#	error_messages = {
#		'required' : 'Please Type'
#	}

#	input_attrs = {
#		'type' : 'text',
#		'placeholder' : 'Nama Kamu'
#	}

#	display_name = forms.CharField(label='', required=False, max_length=27, widget=forms.TextInput(attrs=input_attrs))
